package implementacao;

import interfaces.BaseDeDados;

public class BaseDeDadosImp implements BaseDeDados{

	private float pandak;
	
	@Override
	public boolean conversor(float e) {
		if(e > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public float getValorFinalPandak() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean registraCompra(float valor, String nomeProduto) {
		if(valor>0){
			if(nomeProduto != null){
				if(!nomeProduto.equals("")){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	@Override
	public boolean registraColetaFeitaNoColetor(float valor, String formadepagamento) {
		if(valor>0){
			if(formadepagamento != null){
				if(!formadepagamento.equals("")){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	@Override
	public boolean registraImpressao(String tipo) {
		if(tipo != null){
			if(!tipo.equals("")){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

}
