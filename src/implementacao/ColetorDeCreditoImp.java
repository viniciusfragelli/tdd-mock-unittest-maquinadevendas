package implementacao;

import interfaces.ColetorDeCredito;

public class ColetorDeCreditoImp implements ColetorDeCredito{

	private String forma;
	private float valor;
	
	@Override
	public boolean aprovarPagamento() {
		return true;
	}

	@Override
	public float getValorAprovado() {
		return 10;
	}

	@Override
	public boolean formaDePagamento(String forma) {
		if(forma == null)return false;
		if(forma.equals(""))return false;
		this.forma = forma;
		return true;
	}

	@Override
	public String getNomeFormaDePagamento() {
		return forma;
	}

}
