package implementacao;

import interfaces.Dispensador;

public class DispensadorImp implements Dispensador{

	private float pandak;
	private String produto;
	private int quantidade;
	

	@Override
	public int getQuantidadeProduto() {
		return quantidade;
	}

	@Override
	public boolean setQuantidadeProduto(int quantidade) {
		if(quantidade > 0){
			this.quantidade = quantidade;
			return true;
		}else{
			return false;
		}
		
	}

	@Override
	public boolean setValorProduto(float pandak) {
		if(pandak > 0){
			this.pandak = pandak;
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean verificaSeTemProdutoDisponivel() {
		if(quantidade > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public float getValorProduto() {
		return pandak;
	}

	@Override
	public boolean debitoDeProduto() {
		if(quantidade > 0){
			quantidade--;
			return true;
		}else{
			return false;
		}
	}

	@Override
	public String getNomeDoProduto() {
		return produto;
	}

	@Override
	public boolean setNomeProduto(String nome) {
		if(nome == null)return false;
		if(nome.equals(""))return false;
		this.produto = nome;
		return true;
		
	}

	
}
