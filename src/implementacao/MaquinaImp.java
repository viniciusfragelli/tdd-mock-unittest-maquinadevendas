package implementacao;

import java.util.ArrayList;
import java.util.List;

import interfaces.BaseDeDados;
import interfaces.ColetorDeCredito;
import interfaces.Dispensador;
import interfaces.Imprimir;
import interfaces.Maquina;

public class MaquinaImp implements Maquina{

	private List<Dispensador> itens = new ArrayList<>();
	private List<ColetorDeCredito> coletores = new ArrayList<ColetorDeCredito>();
	private float pandak = 0;
	private BaseDeDados base;
	private Imprimir im;
	
	@Override
	public boolean validaCompra(int k) {
		if(itens.get(k).getQuantidadeProduto() > 0){
			if(itens.get(k).getValorProduto() <= pandak){
				pandak = pandak - itens.get(k).getValorProduto();
				itens.get(k).debitoDeProduto();
				base.registraCompra(itens.get(k).getValorProduto(), itens.get(k).getNomeDoProduto());
				if(pandak == 0){
					return encerraVenda();
				}
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	@Override
	public float getPandakDisponivel() {
		// TODO Auto-generated method stub
		return pandak;
	}

	@Override
	public boolean addPandak(float valor) {
		if(valor > 0){
			pandak += valor;
			return true;
		}else{
			return false;
		}
	}

	@Override
	public void addProdutos(Dispensador d) {
		itens.add(d);
	}

	@Override
	public boolean addMoeda(float valor) {
		if(valor > 0){
			if(base.conversor(valor)){
				pandak += base.getValorFinalPandak();
				return true;
			}else{
				im.imprimePapelErroComValor(valor);
				base.registraImpressao("ERRO");
				return false;
			}
		}else{
			return false;
		}
	}

	@Override
	public void setBase(BaseDeDados base) {
		this.base = base;
	}

	@Override
	public void setImpressora(Imprimir im) {
		this.im = im;
	}

	@Override
	public boolean encerraVenda() {
		if(pandak > 0){
			im.imprimePapelEncerraVendaComPandak(pandak);
			base.registraImpressao("ENCERRA");
			return true;
		}else{
			if(pandak == 0){
				return true;
			}else{
				return false;
			}
		}
	}

	@Override
	public boolean aprovarEReceberDoColetor(int k) {
		if(coletores.get(k).aprovarPagamento()){
			base.registraColetaFeitaNoColetor(coletores.get(k).getValorAprovado(), coletores.get(k).getNomeFormaDePagamento());
			return addMoeda(coletores.get(k).getValorAprovado());
		}else{
			return false;
		}
	}

	@Override
	public void setColetor(List<ColetorDeCredito> coletor) {
		this.coletores = coletor;
	}

	@Override
	public List<Dispensador> getListaDePossiveisCompras() {
		List<Dispensador> d = new ArrayList<Dispensador>();
		for(int i = 0; i < itens.size();i++){
			if(itens.get(i).getValorProduto() <= pandak){
				d.add(itens.get(i));
			}
		}
		return d;
	}

	@Override
	public List<ColetorDeCredito> carregaListaColetorDosClass() {
		//carrega coletor dos class do packet ColetorDeCreditos
		return new ArrayList<ColetorDeCredito>();
	}	
	

}
