package interfaces;

public interface BaseDeDados {

	public boolean conversor(float e);
	public float getValorFinalPandak();
	public boolean registraCompra(float valor, String nomeProduto);
	public boolean registraColetaFeitaNoColetor(float valor, String formadepagamento);
	public boolean registraImpressao(String tipo);
	
}
