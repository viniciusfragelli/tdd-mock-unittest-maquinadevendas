package interfaces;

public interface ColetorDeCredito {
	
	public boolean formaDePagamento(String forma);
	public boolean aprovarPagamento();
	public float getValorAprovado();
	public String getNomeFormaDePagamento();
	
}
