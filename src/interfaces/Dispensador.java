package interfaces;

import java.util.List;

public interface Dispensador {
	
	public int getQuantidadeProduto();
	public boolean setNomeProduto(String nome);
	public boolean setQuantidadeProduto(int quantidade);
	public boolean setValorProduto(float moeda);
	public boolean verificaSeTemProdutoDisponivel();
	public float getValorProduto();
	public boolean debitoDeProduto();
	public String getNomeDoProduto();
	

}
