package interfaces;

public interface Imprimir {
	public void imprimePapelErroComValor(float e);
	public void imprimePapelEncerraVendaComPandak(float p);
}
