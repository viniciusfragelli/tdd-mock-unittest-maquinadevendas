package interfaces;

import java.util.ArrayList;
import java.util.List;

public interface Maquina {
	
	public boolean validaCompra(int k);
	public float getPandakDisponivel();
	public boolean addPandak(float valor);
	public boolean addMoeda(float valor);
	public void addProdutos(Dispensador d);
	public void setBase(BaseDeDados base);
	public void setImpressora(Imprimir im);
	public boolean encerraVenda();
	public boolean aprovarEReceberDoColetor(int k);
	public void setColetor(List<ColetorDeCredito> coletor);
	public List<Dispensador> getListaDePossiveisCompras();
	public List<ColetorDeCredito> carregaListaColetorDosClass();
	
	
}
