package testes;

import implementacao.BaseDeDadosImp;
import implementacao.MaquinaImp;
import interfaces.BaseDeDados;
import interfaces.ColetorDeCredito;
import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Rule;
import org.junit.Test;

public class BancoTest {
	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();

	@Test
	public void testVerificarSeAprovacaoPeloColetorERecusada() {
		
		final BaseDeDados b = new BaseDeDadosImp();
		
		Assert.assertFalse(b.conversor(0));
		Assert.assertFalse(b.conversor(Float.NEGATIVE_INFINITY));
		Assert.assertTrue(b.conversor(1));
		Assert.assertTrue(b.conversor(Float.POSITIVE_INFINITY));
		
		Assert.assertTrue(b.registraColetaFeitaNoColetor(10, "teste"));
		Assert.assertFalse(b.registraColetaFeitaNoColetor(0, "teste"));
		Assert.assertFalse(b.registraColetaFeitaNoColetor(Float.NEGATIVE_INFINITY, "teste"));
		Assert.assertFalse(b.registraColetaFeitaNoColetor(10, ""));
		Assert.assertFalse(b.registraColetaFeitaNoColetor(10, null));		
		
		Assert.assertTrue(b.registraCompra(10, "teste"));
		Assert.assertFalse(b.registraCompra(0, "teste"));
		Assert.assertFalse(b.registraCompra(Float.NEGATIVE_INFINITY, "teste"));
		Assert.assertFalse(b.registraCompra(10, ""));
		Assert.assertFalse(b.registraCompra(10, null));		
		
		Assert.assertTrue(b.registraImpressao("teste"));		
		Assert.assertFalse(b.registraImpressao(""));		
		Assert.assertFalse(b.registraImpressao(null));
		
		
	}
}
