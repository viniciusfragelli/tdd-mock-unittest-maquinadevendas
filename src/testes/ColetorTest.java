package testes;

import java.util.ArrayList;
import java.util.List;

import implementacao.ColetorDeCreditoImp;
import implementacao.MaquinaImp;
import interfaces.BaseDeDados;
import interfaces.ColetorDeCredito;
import interfaces.Dispensador;
import interfaces.Maquina;
import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Rule;
import org.junit.Test;

public class ColetorTest {
	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();

	@Test
	public void testVerificarSeAprovacaoPeloColetorERecusada() {
		
		final ColetorDeCredito coletor1 = context.mock(ColetorDeCredito.class);
		
		final List<ColetorDeCredito> col = new ArrayList<ColetorDeCredito>();
		
		col.add(coletor1);
		
		final Maquina m = new MaquinaImp();
		
		m.setColetor(col);
		
		context.checking(new Expectations(){{
			allowing(coletor1).aprovarPagamento(); will(returnValue(false));
			never(coletor1).getValorAprovado();
		}});
		
		
		boolean teste = m.aprovarEReceberDoColetor(0);
		
		Assert.assertFalse(teste);
		
	}
	
	@Test
	public void testVerificarSeAprovacaoDoColetorCorretamente() {
		
		final ColetorDeCredito coletor1 = context.mock(ColetorDeCredito.class);
		
		final List<ColetorDeCredito> col = new ArrayList<ColetorDeCredito>();
		
		col.add(coletor1);
		
		final Maquina m = new MaquinaImp();
		
		final BaseDeDados base = context.mock(BaseDeDados.class);
		m.setBase(base);
		m.setColetor(col);
		
		context.checking(new Expectations(){{
			allowing(coletor1).aprovarPagamento(); will(returnValue(true));
			allowing(coletor1).getValorAprovado(); will(returnValue((float)20));
			allowing(coletor1).getNomeFormaDePagamento(); will(returnValue("teste"));
			oneOf(base).registraColetaFeitaNoColetor((float)20, "teste");
			oneOf(base).conversor(20); will(returnValue(true));
			oneOf(base).getValorFinalPandak(); will(returnValue((float)20));
		}});
		
		
		boolean teste = m.aprovarEReceberDoColetor(0);
		
		Assert.assertTrue(teste);
		
	}
	
	@Test
    public void testVerificaMetodos() {
		
    	ColetorDeCredito c = new ColetorDeCreditoImp();
    	
		Assert.assertTrue(c.aprovarPagamento());
		Assert.assertFalse(c.formaDePagamento(""));
		Assert.assertFalse(c.formaDePagamento(null));
		Assert.assertTrue(c.formaDePagamento("teste"));
		Assert.assertEquals(c.formaDePagamento("teste"), c.getNomeFormaDePagamento().equals("teste"));
		
	}
}
