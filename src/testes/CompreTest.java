package testes;

import junit.framework.Assert;
import implementacao.MaquinaImp;
import interfaces.BaseDeDados;
import interfaces.Dispensador;
import interfaces.Imprimir;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Rule;
import org.junit.Test;

public class CompreTest {
	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();

	@Test
	public void testVerificarSeDispensadorTemProduto() {
		
		final Dispensador disp = context.mock(Dispensador.class);
		
		final MaquinaImp m = new MaquinaImp();
		
		final BaseDeDados base = context.mock(BaseDeDados.class);
		m.setBase(base);
		
		context.checking(new Expectations(){{ 
			allowing(disp).getValorProduto(); will(returnValue(5));
			allowing(disp).getQuantidadeProduto(); will(returnValue(0));
			never(disp).debitoDeProduto();
			never(base).registraCompra(10, "teste");
		}});
		
		m.addProdutos(disp);
		
		m.addPandak(10);
		boolean teste = m.validaCompra(0);
		
		Assert.assertFalse(teste);
		
	}
	
	@Test
	public void testVerificarSeTemCreditoParaCompra() {
		
		final Dispensador disp = context.mock(Dispensador.class);
		
		final MaquinaImp m = new MaquinaImp();
		
		final BaseDeDados base = context.mock(BaseDeDados.class);
		m.setBase(base);
		
		context.checking(new Expectations(){{ 
			allowing(disp).getValorProduto(); will(returnValue((float)20));
			allowing(disp).getQuantidadeProduto(); will(returnValue(5));
			never(disp).debitoDeProduto();
			never(base).registraCompra(10, "teste");
		}});
		

		m.addPandak(10);
		
		m.addProdutos(disp);
		
		boolean teste = m.validaCompra(0);
		
		Assert.assertFalse(teste);
		
	}
	
	@Test
	public void testVerificarSeCompraEBemSucedida() {
		
		final Dispensador disp = context.mock(Dispensador.class);
		
		final MaquinaImp m = new MaquinaImp();
		
		final BaseDeDados base = context.mock(BaseDeDados.class);
		m.setBase(base);
		
		context.checking(new Expectations(){{ 
			allowing(disp).getValorProduto(); will(returnValue((float)20));
			allowing(disp).getQuantidadeProduto(); will(returnValue(5));
			allowing(disp).getNomeDoProduto(); will(returnValue("teste"));
			oneOf(disp).debitoDeProduto();
			oneOf(base).registraCompra(20 , "teste");
		}});
		

		m.addPandak(30);
		
		m.addProdutos(disp);
		
		boolean teste = m.validaCompra(0);
		
		Assert.assertTrue(teste);
		
	}
	
	@Test
	public void testVerificarSeEncerraACompraAposCompraEBemSucedida() {
		
		final Dispensador disp = context.mock(Dispensador.class);
		final Imprimir im = context.mock(Imprimir.class);
		
		final MaquinaImp m = new MaquinaImp();
		m.setImpressora(im);
		
		final BaseDeDados base = context.mock(BaseDeDados.class);
		m.setBase(base);
		
		context.checking(new Expectations(){{ 
			allowing(disp).getValorProduto(); will(returnValue((float)20));
			allowing(disp).getQuantidadeProduto(); will(returnValue(5));
			allowing(disp).getNomeDoProduto(); will(returnValue("teste"));
			oneOf(disp).debitoDeProduto();
			oneOf(base).registraCompra(20 , "teste");
			never(im).imprimePapelEncerraVendaComPandak(0);
		}});
		

		m.addPandak(20);
		
		m.addProdutos(disp);
		
		boolean teste = m.validaCompra(0);
		
		Assert.assertTrue(teste);
		
	}

}
