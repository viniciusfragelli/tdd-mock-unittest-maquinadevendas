package testes;

import implementacao.DispensadorImp;
import implementacao.MaquinaImp;
import interfaces.Dispensador;
import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Test;

public class DispensadorTest {
	public JUnitRuleMockery context = new JUnitRuleMockery();

	@Test
	public void testVerificarSeDispensadorTemProduto() {
		
		final Dispensador disp = new DispensadorImp();
		
		Assert.assertFalse(disp.setValorProduto(0));
		Assert.assertFalse(disp.setValorProduto(Float.NEGATIVE_INFINITY));
		Assert.assertTrue(disp.setValorProduto(1));
		Assert.assertTrue(disp.setValorProduto(Float.POSITIVE_INFINITY));
		
		Assert.assertFalse(disp.setQuantidadeProduto(0));
		Assert.assertFalse(disp.setQuantidadeProduto(Integer.MIN_VALUE));
		Assert.assertTrue(disp.setQuantidadeProduto(1));
		Assert.assertTrue(disp.setQuantidadeProduto(Integer.MAX_VALUE));
		
		Assert.assertEquals(disp.setValorProduto(1), (disp.getValorProduto() == 1));
		Assert.assertEquals(disp.setValorProduto(10), (disp.getValorProduto() == 10));
		
		Assert.assertEquals(disp.setQuantidadeProduto(10), (disp.getQuantidadeProduto() == 10));
		Assert.assertEquals(disp.setQuantidadeProduto(1), (disp.getQuantidadeProduto() == 1));
		
		Assert.assertFalse(disp.setNomeProduto(""));
		Assert.assertFalse(disp.setNomeProduto(null));
		Assert.assertTrue(disp.setNomeProduto("teste"));
		
		Assert.assertEquals(disp.setNomeProduto("teste"),("teste".equals(disp.getNomeDoProduto())));
		
		Assert.assertEquals(disp.setQuantidadeProduto(1), disp.debitoDeProduto());
		
		disp.setQuantidadeProduto(1);
		disp.debitoDeProduto();
		Assert.assertFalse(disp.debitoDeProduto());
		
	}
}
