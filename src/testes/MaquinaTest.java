package testes;

import implementacao.MaquinaImp;
import interfaces.BaseDeDados;
import interfaces.Dispensador;
import interfaces.Imprimir;
import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Rule;
import org.junit.Test;

public class MaquinaTest {
	@Rule
	public JUnitRuleMockery context = new JUnitRuleMockery();

	@Test
	public void testVerificarSeBancoECahamadoComValorInvalidos() {
		
		final MaquinaImp m = new MaquinaImp();
		
		final BaseDeDados base = context.mock(BaseDeDados.class);
		m.setBase(base);
		
		final Imprimir im = context.mock(Imprimir.class);
		m.setImpressora(im);
		
		context.checking(new Expectations(){{
			never(base).conversor(0);
			never(im).imprimePapelErroComValor(0);
		}});
		
		
		boolean teste = m.addMoeda(0);
		
		Assert.assertFalse(teste);
		
		teste = m.addMoeda(Float.NEGATIVE_INFINITY);
		
		Assert.assertFalse(teste);
		
	}
	
	@Test
	public void testVerificarSeImprimeCasoBancoFalhe() {
		
		final MaquinaImp m = new MaquinaImp();
		
		final BaseDeDados base = context.mock(BaseDeDados.class);
		m.setBase(base);
		
		final Imprimir im = context.mock(Imprimir.class);
		m.setImpressora(im);
		
		context.checking(new Expectations(){{
			oneOf(base).conversor(1); will(returnValue(false));
			oneOf(im).imprimePapelErroComValor(1);
			never(base).getValorFinalPandak();
			never(im).imprimePapelEncerraVendaComPandak(1);
			oneOf(base).registraImpressao("ERRO");
		}});
		
		
		boolean teste = m.addMoeda(1);
		
		Assert.assertFalse(teste);
		
	}
	
	@Test
	public void testVerificarNaoImprimeCasoBancoRetorneESeBancoEChamadoUmaVez() {
		
		final MaquinaImp m = new MaquinaImp();
		
		final BaseDeDados base = context.mock(BaseDeDados.class);
		m.setBase(base);
		
		final Imprimir im = context.mock(Imprimir.class);
		m.setImpressora(im);
		
		context.checking(new Expectations(){{
			oneOf(base).conversor(1); will(returnValue(true));
			never(im).imprimePapelErroComValor(1);
			oneOf(base).getValorFinalPandak();
			never(im).imprimePapelEncerraVendaComPandak(1);
		}});
		
		
		boolean teste = m.addMoeda(1);
		
		Assert.assertTrue(teste);
		
	}
	
	@Test
	public void testVerificarSeValorEmDinheiroECreditadoCorretamente() {
		
		final MaquinaImp m = new MaquinaImp();
		
		final BaseDeDados base = context.mock(BaseDeDados.class);
		m.setBase(base);
		
		final Imprimir im = context.mock(Imprimir.class);
		m.setImpressora(im);
		
		
		context.checking(new Expectations(){{
			oneOf(base).conversor(1); will(returnValue(true));
			oneOf(base).getValorFinalPandak(); will(returnValue((float)10));
		}});
		
		
		m.addMoeda(1);
		
		Assert.assertTrue((m.getPandakDisponivel() == 10));
		
		
	}
	
	@Test
	public void testVerificarImprimirEncerraCompras() {
		
		final MaquinaImp m = new MaquinaImp();
		
		final BaseDeDados base = context.mock(BaseDeDados.class);
		m.setBase(base);
		
		final Imprimir im = context.mock(Imprimir.class);
		m.setImpressora(im);
		
		m.addPandak(10);
		
		context.checking(new Expectations(){{
			oneOf(im).imprimePapelEncerraVendaComPandak(10);
			oneOf(base).registraImpressao("ENCERRA");
			never(im).imprimePapelErroComValor(0);
		}});
		
		
		boolean teste = m.encerraVenda();
		
		Assert.assertTrue(teste);
		
		
	}
	
	@Test
	public void testVerificarSeExisteCreditoAoEncerraComprasESeNaoImprime() {
		
		final MaquinaImp m = new MaquinaImp();
		final BaseDeDados base = context.mock(BaseDeDados.class);
		m.setBase(base);
		final Imprimir im = context.mock(Imprimir.class);
		m.setImpressora(im);
		
		m.addPandak(0);
		
		context.checking(new Expectations(){{
			never(im).imprimePapelEncerraVendaComPandak(0);
			never(im).imprimePapelErroComValor(0);
			never(base).registraImpressao("ENCERRA");
		}});
		
		
		boolean teste = m.encerraVenda();
		
		Assert.assertTrue(teste);
		
		
	}
	
}
